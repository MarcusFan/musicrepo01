package com.example.moosic;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    void OnUserInput(int position){
        switchSong(currentSongIndex, position);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //pausesong();

        populatedataModel();

        connectXml();

        setuprecycle();

        setupbuttonHandlers();

        displayLogo();



    }

//    void pausesong(){
//        mediaPlayer.pause();
//    }

    void populatedataModel(){


        playlist.name = "The Playlist";
        playlist.songs = new ArrayList<Song>();

        Song song = new Song();
        song.songName = "Over The Sun";
        song.artistName = "NoCopyrightSounds";
        song.imageResource = R.drawable.coopexoverthesun;
        song.mp3Resource = R.raw.overthesun;
        playlist.songs.add(song);

        //Next Song
        song = new Song();
        song.songName = "Acoustic Breeze";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.acousticbreeze;
        song.mp3Resource = R.raw.acousticbreeze;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "A new beginning";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.anewbeginning;
        song.mp3Resource = R.raw.anewbeginning;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Creative Minds";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.creativeminds;
        song.mp3Resource = R.raw.creativeminds;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Going Higher";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.goinghigher;
        song.mp3Resource = R.raw.goinghigher;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Happy Rock";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.happyrock;
        song.mp3Resource = R.raw.happyrock;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Hey!";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.hey;
        song.mp3Resource = R.raw.hey;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Summer";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.summer;
        song.mp3Resource = R.raw.summer;

        playlist.songs.add(song);
    }

    void connectXml(){
        songsrecyclerView = findViewById(R.id.SongList);
        imageView = findViewById(R.id.imageView);
        songnameTextView = findViewById(R.id.SongName);
        artistnameTextView = findViewById(R.id.ArtistName);
        previousbutton = findViewById(R.id.previous);
        playbutton = findViewById(R.id.play);
        pausebutton = findViewById(R.id.pause);
        nextbutton = findViewById(R.id.next);
    }

    void setuprecycle() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        songsrecyclerView.setLayoutManager(layoutManager);

        songAdapter = new SongAdapter(this, playlist.songs, this);
        songsrecyclerView.setAdapter(songAdapter);
    }

    void displayCurrentsong(){
        Song currentSong = playlist.songs.get(currentSongIndex);
        imageView.setImageResource(currentSong.imageResource);
        songnameTextView.setText(currentSong.songName);
        artistnameTextView.setText(currentSong.artistName);
    }

    void setupbuttonHandlers() {

        previousbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentSongIndex -1 >= 0) {
                    switchSong(currentSongIndex, currentSongIndex - 1);

                }else if (currentSongIndex == 0){
                    switchSong(currentSongIndex, currentSongIndex = 7);
                }
            }
        });

        nextbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentSongIndex + 1 < playlist.songs.size()) {
                    switchSong(currentSongIndex, currentSongIndex + 1);


                }else if (currentSongIndex == 7){
                    switchSong(currentSongIndex,currentSongIndex = 0);
                }
            }
        });


        pausebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pauseCurrentsong();
            }
        });


        playbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                playCurrentsong();

            }
        });
    }

    void switchSong(int fromIndex, int toIndex){
        songAdapter.notifyItemChanged(currentSongIndex);
        currentSongIndex = toIndex;

        displayCurrentsong();

        songAdapter.notifyItemChanged(currentSongIndex);

        songsrecyclerView.scrollToPosition(currentSongIndex);

        if (mediaPlayer != null && mediaPlayer.isPlaying()){
            pauseCurrentsong();

            mediaPlayer = null;

            playCurrentsong();

        }else {
            mediaPlayer = null;
        }


    }

    @SuppressLint("SetTextI18n")
    void displayLogo(){
        Ree = R.drawable.logoimage;
        imageView.setImageResource(Ree);
        songnameTextView.setText("Stinky Music");
        artistnameTextView.setText("Team-2-04");
        mediaPlayer = null;
    }

    void pauseCurrentsong(){

        System.out.println("Paused at song Index " + currentSongIndex);
        if (mediaPlayer != null){
            mediaPlayer.pause();

        }
    }

    void playCurrentsong(){

        System.out.println("Playing at song Index " + currentSongIndex);
        if (mediaPlayer == null){
            Song currentSong = playlist.songs.get(currentSongIndex);
            mediaPlayer = MediaPlayer.create(MainActivity.this, currentSong.mp3Resource);

        }
        mediaPlayer.start();
    }

    // Properties

    Playlist playlist = new Playlist();
    SongAdapter songAdapter;
    Integer currentSongIndex = 0;

    //Media
    MediaPlayer mediaPlayer = null;

    //XML
    RecyclerView songsrecyclerView;
    ImageView imageView;
    TextView songnameTextView;
    TextView artistnameTextView;
    ImageButton previousbutton;
    ImageButton playbutton;
    ImageButton pausebutton;
    ImageButton nextbutton;
    int Ree;
    boolean title = true;



}