package com.example.moosic;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SongViewholder extends RecyclerView.ViewHolder {

    // Constructor
    public SongViewholder(@NonNull View itemView) {
        super(itemView);

        this.itemView = itemView;
        imageView = itemView.findViewById(R.id.Image_View);
        songnameText = itemView.findViewById(R.id.Song_name_text);
        artistnameText = itemView.findViewById(R.id.Artist_name_text);
        selectedSongView = itemView.findViewById(R.id.selectedSongView);
    }

    //Properties
    View itemView;
    ImageView imageView;
    TextView songnameText;
    TextView artistnameText;
    View selectedSongView;
}
