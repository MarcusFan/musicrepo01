package com.example.moosic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SongAdapter extends RecyclerView.Adapter<SongViewholder> {

    //Constructor
    SongAdapter(@NonNull Context context, @NonNull ArrayList<Song> songs, @NonNull MainActivity mainActivity){
        this.context = context;
        this.songs = songs;
        this.mainActivity = mainActivity;
    }


    //Methods
    @NonNull
    @Override
    public SongViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_song, parent, false);
        SongViewholder viewholder  = new SongViewholder(itemView);
        return viewholder;
    //ViewHolder needs to be used
    }

    @Override
    public void onBindViewHolder(@NonNull SongViewholder holder, int position) {
    //Used if ViewHolder needs to be reused
        Song song = songs.get(position);
        holder.imageView.setImageResource(song.imageResource);
        holder.songnameText.setText(song.songName);
        holder.artistnameText.setText(song.artistName);
        if (position == mainActivity.currentSongIndex) {
            holder.selectedSongView.setVisibility(View.VISIBLE);
        }else{
            holder.selectedSongView.setVisibility(View.INVISIBLE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.OnUserInput(position);

            }
        });
    }

    @Override
    public int getItemCount() {
        //RecyclerView needs total items
        return songs.size();
    }

    //Properties
    Context context;
    ArrayList<Song> songs;
    MainActivity mainActivity;
}
